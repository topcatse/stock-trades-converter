from setuptools import setup

setup(
    name='tradesconverter',
    packages=['tradesconverter'],
    include_package_data=True,
    install_requires=[
        'flask',
        'freezegun',
    ],
)
