FROM python:3.7-alpine


##
# Environment variables
##
ENV VIRTUAL_HOST=stocks.docker


##
# Set Swedish time zon
##
RUN apk add tzdata && \
    ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime


##
# Install requirements
##
COPY setup.py /app/setup.py
COPY tradesconverter/__init__.py /app/tradesconverter/__init__.py
WORKDIR /app
RUN pip install --editable .


##
# Copy source files
##
COPY . /app


##
# Run tests
##
RUN python tradesconverter/tests/app/test_convert.py && \
    python tradesconverter/tests/utils/test_dates.py && \
    python tradesconverter/tests/utils/test_files.py

##
# Run app
##
CMD ["python", "tradesconverter/app.py"]
