# Stock Trades Converter

A REST app ([stock-trades-converter.stocksholm.com](http://stock-trades-converter.stocksholm.com/)) to convert Stock Trades from CSV to SRU format, used by [Skatteverket](https://www.skatteverket.se/).

In this example, the [trades.csv](examples/trades.csv) file is converted to [BLANKETTER.SRU](examples/BLANKETTER.SRU). It can be uploaded to [Skatteverket](https://www.skatteverket.se/foretagochorganisationer/sjalvservice/allaetjanster/tjanster/filoverforing) together with the [INFO.SRU](examples/INFO.SRU) file under:

1. Filöverföring >> Testa filer
1. Click: Inkomstdeklaration
1. Click: Överföra filer
1. Select both [INFO.SRU](examples/INFO.SRU) and [BLANKETTER.SRU](examples/BLANKETTER.SRU)

## Sensitivity

No data is ever saved. Both the uploaded CSV file and the generated SRU files are instantly deleted once the convert is done.

## 1. Export CSV file

The first step is to export a CSV file.

### Interactive Brokers

Below follows a step by step guide on how to generate a CSV file from
[Interactive Brokers](https://www.interactivebrokers.co.uk/) (see the [trades.csv example](examples/trades.csv)).

1. In Account Management, navigate to:
    - Reports / Tax Docs » Flex Queries » Activity Flex Query
1. Click the "+" icon to create a new report:
    - Activity Flex Query Details
        - Query Name: Trades
    - Sections
        - Select: Trades
            - Options: Symbol Summary
            - Fields (in this order):
                1. Symbol
                2. Buy/Sell
                3. Quantity
                4. TradePrice
                5. IB Commission
                6. Currency
        - Click: Save
    - Delivery Configuration
        - Models: Optional
        - Format: CSV
        - Include header and trailer records?: No
        - Include column headers?: Yes
        - Include section code and line descriptor?: No
        - Period: Last Business Day
    - General Configuration
        - Date Format: yyyy-MM-dd
        - Time Format: HH:mm:ss
        - Date/Time Separator: ' ' (single-space)
        - Profit and Loss: Default
        - Include Canceled Trades?: No
        - Include Currency Rates?: No
        - Display Account Alias in Place of Account ID?: No
1. Click: Continue
1. Click: Create
1. Click the "Run" button next to the report
    - Period: Custom Date Range
    - Format: CSV
1. Wait and download the file
    - If necessary, rename the extension from `.txt` to `.csv`

## 2. Generate BLANKETTER.SRU

1. Navigate to: http://stock-trades-converter.stocksholm.com/blanketter
1. Select the CSV file
1. Enter a Social Security Number
    - It is only used to generate the .SRU file, it never saved anywhere
1. Click: Generate

## 3. Generate INFO.SRU

1. Navigate to: http://stock-trades-converter.stocksholm.com/info
1. Fill in the fields
1. Click: Generate

## To think about

1. CSV file should container header columns in the above mentioned order

1. Order type should be either `BUY` or `SELL`
