import os
from flask import Flask, request, redirect, render_template, send_from_directory

from tradesconverter.app.convert import is_valid_ssn, is_valid_conversion_rate, get_sru_options, generate_blanketter_sru, generate_csv, generate_info_sru, get_profit_and_loss
from tradesconverter.utils import files, dates

UPLOAD_FOLDER = '/app'
BLANKETTER_FILENAME = 'BLANKETTER.SRU'
INFO_FILENAME = 'INFO.SRU'
CONVERTED_FILENAME = 'TRADES_PROFIT_LOSS.CSV'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/blanketter')
def blanketter():
    return render_template('form_blanketter.html')


@app.route('/info')
def info():
    return render_template('form_info.html')


@app.route('/pnl')
def pnl():
    return render_template('form_pnl.html')


@app.route('/convert')
def convert():
    return render_template('form_convert.html')


@app.route('/generate_blanketter', methods=['POST'])
def generate_blanketter():
    if request.method != 'POST':
        return redirect('/blanketter')

    timestamp = dates.get_timestamp()
    is_csv_valid, csv_file, csv_filename = files.validate_csv_file(request, timestamp)
    if not is_csv_valid:
        return redirect('/blanketter')

    conversion_rate = request.form['conversion_rate']
    ssn = request.form['ssn']

    if not is_valid_ssn(ssn) or not is_valid_conversion_rate(conversion_rate):
        return redirect('/blanketter')

    sru_options = get_sru_options(conversion_rate, ssn)

    csv_file.save(os.path.join(app.config['UPLOAD_FOLDER'], csv_filename))
    try:
        timestamp_filename = '%s-%s' % (timestamp, BLANKETTER_FILENAME)
        generate_blanketter_sru(timestamp_filename, csv_filename, sru_options)
    finally:
        os.remove(csv_filename)

    return redirect('/download/%s/%s' % (timestamp_filename, BLANKETTER_FILENAME))


@app.route('/convert_csv', methods=['POST'])
def convert_csv():
    if request.method != 'POST':
        return redirect('/convert')

    timestamp = dates.get_timestamp()
    is_csv_valid, csv_file, csv_filename = files.validate_csv_file(request, timestamp)
    if not is_csv_valid:
        return redirect('/convert')

    conversion_rate = request.form['conversion_rate']
    if not is_valid_conversion_rate(conversion_rate):
        return redirect('/convert')

    sru_options = get_sru_options(conversion_rate)

    csv_file.save(os.path.join(app.config['UPLOAD_FOLDER'], csv_filename))
    try:
        timestamp_filename = '%s-%s' % (timestamp, CONVERTED_FILENAME)
        generate_csv(timestamp_filename, csv_filename, sru_options)
    finally:
        os.remove(csv_filename)

    return redirect('/download/%s/%s' % (timestamp_filename, CONVERTED_FILENAME))


@app.route('/calculate_pnl', methods=['POST'])
def calculate_pnl():
    if request.method != 'POST':
        return redirect('/pnl')

    timestamp = dates.get_timestamp()
    is_csv_valid, csv_file, csv_filename = files.validate_csv_file(request, timestamp)
    if not is_csv_valid:
        return redirect('/pnl')

    conversion_rate = request.form['conversion_rate']
    if not is_valid_conversion_rate(conversion_rate):
        return redirect('/pnl')

    sru_options = get_sru_options(conversion_rate)

    csv_file.save(os.path.join(app.config['UPLOAD_FOLDER'], csv_filename))
    try:
        profit_loss = get_profit_and_loss(csv_filename, sru_options)
    finally:
        os.remove(csv_filename)

    return str(profit_loss)


@app.route('/generate_info', methods=['POST'])
def generate_info():
    if request.method != 'POST':
        return redirect('/info')

    ssn = request.form['ssn']
    if not is_valid_ssn(ssn):
        return redirect('/info')

    timestamp = dates.get_timestamp()
    sru_filename = '%s-%s' % (timestamp, INFO_FILENAME)
    sru_options = {
        'ssn': 'ORGNR',
        'name': 'NAMN',
        'address': 'ADRESS',
        'zip': 'POSTNR',
        'city': 'POSTORT',
        'email': 'EMAIL',
    }

    try:
        generate_info_sru(sru_filename, sru_options, request.form)
    except:
        os.remove(sru_filename)

    return redirect('/download/%s/%s' % (sru_filename, INFO_FILENAME))


@app.route('/download/<path:filename>/<path:alias>')
def download_file(filename, alias):
    @app.after_request
    def remove_file(response):
        if os.path.exists(filename):
            os.remove(filename)
        return response

    return send_from_directory(
        app.config['UPLOAD_FOLDER'],
        filename,
        as_attachment=True,
        attachment_filename=alias,
    )


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
