import unittest

from freezegun import freeze_time
from tradesconverter.app import convert


class AppConvertTest(unittest.TestCase):

    def test_is_valid_ssn_should_return_true_if_correct_format(self):
        ssn = '195012310000'
        result = convert.is_valid_ssn(ssn)
        self.assertTrue(result)

    def test_is_valid_ssn_should_return_false_if_less_than_12_numbers(self):
        ssn = '5012310000'
        result = convert.is_valid_ssn(ssn)
        self.assertFalse(result)

    def test_is_valid_ssn_should_return_false_if_contains_other_than_numbers(self):
        ssn = 'a95012310000'
        result = convert.is_valid_ssn(ssn)
        self.assertFalse(result)

    def test_is_valid_ssn_should_return_false_if_contains_other_than_numbers_in_end(self):
        ssn = '19501231aaaa'
        result = convert.is_valid_ssn(ssn)
        self.assertFalse(result)

    def test_is_valid_conversion_rate_should_return_true_if_correct_format(self):
        ssn = '7.84'
        result = convert.is_valid_conversion_rate(ssn)
        self.assertTrue(result)

    def test_is_valid_conversion_rate_should_return_false_if_incorrect_format(self):
        ssn = '7,84'
        result = convert.is_valid_conversion_rate(ssn)
        self.assertFalse(result)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_sru_options_should_include_current_time(self):
        conversion_rate = '7.84'
        ssn = '195012310000'
        expected = "112233"
        result = convert.get_sru_options(conversion_rate, ssn)
        self.assertEqual(result['time'], expected)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_sru_options_should_include_current_date(self):
        conversion_rate = '7.84'
        ssn = '195012310000'
        expected = "20120114"
        result = convert.get_sru_options(conversion_rate, ssn)
        self.assertEqual(result['date'], expected)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_sru_options_should_include_previous_year(self):
        conversion_rate = '7.84'
        ssn = '195012310000'
        expected = "2011"
        result = convert.get_sru_options(conversion_rate, ssn)
        self.assertEqual(result['year'], expected)

    def test_get_sru_options_should_include_social_security_number(self):
        conversion_rate = '7.84'
        ssn = '195012310000'
        expected = "195012310000"
        result = convert.get_sru_options(conversion_rate, ssn)
        self.assertEqual(result['ssn'], expected)

    def test_get_sru_options_should_include_social_security_number_by_default(self):
        conversion_rate = '7.84'
        expected = "195012310000"
        result = convert.get_sru_options(conversion_rate)
        self.assertEqual(result['ssn'], expected)

    def test_get_sru_options_should_include_conversion_rate(self):
        conversion_rate = '7.84'
        ssn = '195012310000'
        expected = 7.84
        result = convert.get_sru_options(conversion_rate, ssn)
        self.assertEqual(result['conversion_rate'], expected)

    def test_get_trades_should_normalize_csv_content(self):
        reader = [
            {
                0: 'Symbol',
                1: 'Buy/Sell',
                2: 'Quantity',
                3: 'TradePrice',
                4: 'IBCommission',
                5: 'CurrencyPrimary',
            },
            {
                0: 'AAPL',
                1: 'BUY',
                2: '66',
                3: '217.745',
                4: '-0.407369785',
                5: 'USD',
            },
            {
                0: 'AAPL',
                1: 'SELL',
                2: '-66',
                3: '215.1796',
                4: '-0.553647882',
                5: 'USD',
            },
        ]

        sru_options = {
            'conversion_rate': 7.84,
        }

        expected = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 66,
                'TradePrice': 217.745,
                'Commission': -0.407369785,
                'Currency': 'USD',
                'ConversionRate': 7.84,
            },
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 66,
                'TradePrice': 215.1796,
                'Commission': -0.553647882,
                'Currency': 'USD',
                'ConversionRate': 7.84,
            },
        ]

        result = convert.get_trades(reader, sru_options)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_return_loss_as_positive_integer(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 66,
                'TradePrice': 217.745,
                'Commission': -0.407369785,
                'Currency': 'USD',
                'ConversionRate': 7.84,
            },
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 66,
                'TradePrice': 215.1796,
                'Commission': -0.553647882,
                'Currency': 'USD',
                'ConversionRate': 7.84,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 66,
                'SellCost': 111338,
                'BuyCost': 112673,
                'Profit': 0,
                'Loss': 1335,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_return_profit_as_positive_integer(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 66,
                'TradePrice': 217.745,
                'Commission': -0.407369785,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 66,
                'TradePrice': 220.1796,
                'Commission': -0.553647882,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 66,
                'SellCost': 145313,
                'BuyCost': 143716,
                'Profit': 1597,
                'Loss': 0,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_return_profit_and_loss_as_zero(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 66,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 66,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 66,
                'SellCost': 66000,
                'BuyCost': 66000,
                'Profit': 0,
                'Loss': 0,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_return_average_quantity(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 60,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 40,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 50,
                'SellCost': 40000,
                'BuyCost': 60000,
                'Profit': 0,
                'Loss': 20000,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_work_if_only_buy_trade(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'BUY',
                'Quantity': 60,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 60,
                'SellCost': 0,
                'BuyCost': 60000,
                'Profit': 0,
                'Loss': 60000,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_symbols_by_trades_should_work_if_only_sell_trade(self):
        trades = [
            {
                'Symbol': 'AAPL',
                'Type': 'SELL',
                'Quantity': 60,
                'TradePrice': 100,
                'Commission': 0,
                'Currency': 'USD',
                'ConversionRate': 10,
            },
        ]

        expected = {
            'AAPL': {
                'Quantity': 60,
                'SellCost': 60000,
                'BuyCost': 0,
                'Profit': 60000,
                'Loss': 0,
            },
        }

        result = convert.get_symbols_by_trades(trades)
        self.assertEqual(result, expected)

    def test_get_pnl_should_return_profit_or_loss(self):
        reader = [
            {
                0: 'Symbol',
                1: 'Buy/Sell',
                2: 'Quantity',
                3: 'TradePrice',
                4: 'IBCommission',
                5: 'CurrencyPrimary',
            },
            {
                0: 'AAPL',
                1: 'BUY',
                2: '66',
                3: '217.745',
                4: '-0.407369785',
                5: 'USD',
            },
            {
                0: 'AAPL',
                1: 'SELL',
                2: '-66',
                3: '215.1796',
                4: '-0.553647882',
                5: 'USD',
            },
            {
                0: 'AMZN',
                1: 'BUY',
                2: '134',
                3: '1593.923481716',
                4: '-11.576243073',
                5: 'USD',
            },
            {
                0: 'AMZN',
                1: 'SELL',
                2: '-134',
                3: '1614.378170896',
                4: '-12.863659265',
                5: 'USD',
            },
        ]

        sru_options = {
            'conversion_rate': 7.84,
        }

        expected = 19962

        result = convert.get_pnl(reader, sru_options)
        self.assertEqual(result, expected)

    def test_get_selling_price_should_return_0_if_type_is_buy(self):
        row = {
            'Type': 'BUY',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': -0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 0
        result = convert.get_selling_price(row)
        self.assertEqual(result, expected)

    def test_get_selling_price_should_multiply_quantity_with_trade_price_and_subtract_commission_if_type_is_sell(self):
        row = {
            'Type': 'SELL',
            'Quantity': 66,
            'TradePrice': 215.1796,
            'Commission': -0.553647882,
            'ConversionRate': 8.1743820580,
        }
        expected = 116087
        result = convert.get_selling_price(row)
        self.assertEqual(result, expected)

    def test_get_selling_price_should_support_positive_commission(self):
        row = {
            'Type': 'SELL',
            'Quantity': 66,
            'TradePrice': 215.1796,
            'Commission': 0.553647882,
            'ConversionRate': 8.1743820580,
        }
        expected = 116087
        result = convert.get_selling_price(row)
        self.assertEqual(result, expected)

    def test_get_buying_price_should_return_0_if_type_is_sell(self):
        row = {
            'Type': 'SELL',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': -0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 0
        result = convert.get_buying_price(row)
        self.assertEqual(result, expected)

    def test_get_buying_price_should_multiply_quantity_with_trade_price_and_add_commission_if_type_is_buy(self):
        row = {
            'Type': 'BUY',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': -0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 117479
        result = convert.get_buying_price(row)
        self.assertEqual(result, expected)

    def test_get_buying_price_should_support_positive_commission(self):
        row = {
            'Type': 'BUY',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': 0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 117479
        result = convert.get_buying_price(row)
        self.assertEqual(result, expected)

    def test_get_profit_should_return_0_if_type_is_buy(self):
        row = {
            'Type': 'BUY',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': -0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 0
        result = convert.get_profit(row)
        self.assertEqual(result, expected)

    def test_get_profit_should_return_selling_price_if_type_is_sell(self):
        row = {
            'Type': 'SELL',
            'Quantity': 66,
            'TradePrice': 215.1796,
            'Commission': 0.553647882,
            'ConversionRate': 8.1743820580,
        }
        expected = 116087
        result = convert.get_profit(row)
        self.assertEqual(result, expected)

    def test_get_loss_should_return_0_if_type_is_sell(self):
        row = {
            'Type': 'SELL',
            'Quantity': 66,
            'TradePrice': 217.745,
            'Commission': -0.407369785,
            'ConversionRate': 8.1743820580,
        }
        expected = 0
        result = convert.get_loss(row)
        self.assertEqual(result, expected)

    def test_get_loss_should_return_buying_price_if_type_is_buy(self):
        row = {
            'Type': 'BUY',
            'Quantity': 66,
            'TradePrice': 215.1796,
            'Commission': 0.553647882,
            'ConversionRate': 8.1743820580,
        }
        expected = 116096
        result = convert.get_loss(row)
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
