import unittest

from freezegun import freeze_time
from tradesconverter.utils import dates


class UtilsDatesTest(unittest.TestCase):

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_timestamp_should_format_date_with_milliseconds_as_default(self):
        expected = "2012-01-14_11-22-33-123456"
        result = dates.get_timestamp()
        self.assertEqual(result, expected)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_timestamp_should_support_custom_date_format(self):
        date_format = '%Y%m%d'
        expected = "20120114"
        result = dates.get_timestamp(date_format)
        self.assertEqual(result, expected)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_timestamp_should_support_custom_time_format(self):
        time_format = '%H%M%S'
        expected = "112233"
        result = dates.get_timestamp(time_format)
        self.assertEqual(result, expected)

    @freeze_time("2012-01-14 11:22:33.123456")
    def test_get_previous_year_should_return_previous_year(self):
        expected = "2011"
        result = dates.get_previous_year()
        self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
