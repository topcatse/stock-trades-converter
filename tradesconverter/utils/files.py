from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['csv'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def validate_csv_file(request, timestamp):
    if 'file' not in request.files:
        return False, None, None

    csv_file = request.files['file']
    uploaded_filename = secure_filename(csv_file.filename)
    if uploaded_filename == '':
        return False, csv_file, None

    csv_filename = '%s-%s' % (timestamp, uploaded_filename)
    if not allowed_file(csv_filename):
        return False, csv_file, csv_filename

    return True, csv_file, csv_filename
