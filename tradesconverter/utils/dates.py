import datetime


def get_timestamp(date_format='%Y-%m-%d_%H-%M-%S-%f'):
    return datetime.datetime.now().strftime(date_format)


def get_previous_year():
    current_year = get_timestamp('%Y')
    return str(int(current_year) - 1)
